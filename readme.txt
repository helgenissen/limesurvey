mobile optimized template for limesurvey.
-------------------------------------------


Provides automatic forwarding. 
The format question by question should be set.

Supported question types:

- option lists
- checkbox lists
- short free text
- long free text
- matrix (e.g. 1-5, 1-7) with 1 subquestion